package com.example.myapplication;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;
import android.widget.TextView;

import org.w3c.dom.Text;

public class MainActivity extends AppCompatActivity implements View.OnClickListener{

    Button btnAdd, btnRead,btnClear,btnUpdate,btnDelete;
    EditText editName,editMail,editID;
    GridView userGrid;

    DBHelper dbHelper;

    TextView textDown;

    SimpleCursorAdapter adapter;
    Cursor cursor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        btnAdd = (Button) findViewById(R.id.button);
        btnAdd.setOnClickListener(this);

        btnRead = (Button) findViewById(R.id.button2);
        btnRead.setOnClickListener(this);

        btnClear = (Button) findViewById(R.id.button3);
        btnClear.setOnClickListener(this);

        btnUpdate = (Button) findViewById(R.id.button4);
        btnUpdate.setOnClickListener(this);

        btnDelete = (Button) findViewById(R.id.button5);
        btnDelete.setOnClickListener(this);

        editName = (EditText)findViewById(R.id.editText);
        editMail = (EditText)findViewById(R.id.editText3);
        editID = (EditText)findViewById(R.id.editText2);
        textDown = (TextView) findViewById(R.id.tv);

        userGrid = (GridView) findViewById(R.id.grid);


        dbHelper = new DBHelper(this);

        }

     @Override
     public void onClick (View v){

        String name = editName.getText().toString();
        String email = editMail.getText().toString();
        String id = editID.getText().toString();



         SQLiteDatabase database = dbHelper.getWritableDatabase();

         ContentValues values = new ContentValues();


        switch (v.getId()){

            case R.id.button:
                if (!name.equalsIgnoreCase("")&&(!email.equalsIgnoreCase(""))){
                    values.put(DBHelper.KEY_NAME,name);
                    values.put(DBHelper.KEY_MAIL,email);


                editName.getText().clear();
                editMail.getText().clear();

                database.insert(DBHelper.TABLE_CONTACTS,null, values);
                    textDown.setText(" Запись добавлена в базу!!!");}
                else textDown.setText(" Одно из полей не заполнено!!!");

                break;




            case R.id.button2:
            cursor = database.query(DBHelper.TABLE_CONTACTS,null,null,null,null,null, null);
//
//                if (cursor.moveToFirst()){
//
//                    int idIndex = cursor.getColumnIndex(DBHelper.KEY_ID);
//                    int nameIndex = cursor.getColumnIndex(DBHelper.KEY_NAME);
//                    int emailIndex = cursor.getColumnIndex(DBHelper.KEY_MAIL);
//
//                    while (!cursor.isAfterLast()){
//                        Log.d("mLog", "ID = "+ cursor.getInt(idIndex) +
//                                ", name = "+ cursor.getString(nameIndex) +
//                                ", email = " + cursor.getString(emailIndex));
//
//                        cursor.moveToNext();
//                    }
//
//                }
//                 if (cursor.isAfterLast()){
//                     textDown.setText(" 0 rows" );
//                 }

                //cursor = database.rawQuery("select * from "+ DBHelper.TABLE_CONTACTS,null);
                textDown.setText("Найдено элементов: " + String.valueOf(cursor.getCount()));

                String[] from = new String[] {DBHelper.KEY_NAME, DBHelper.KEY_MAIL};
                adapter = new SimpleCursorAdapter(this, android.R.layout.two_line_list_item,
                        cursor, from, new int[]{android.R.id.text1, android.R.id.text2}, 0);
                userGrid.setAdapter(adapter);  // вывод данных из адаптера в лист

                break;



            case R.id.button3:
                database.delete(DBHelper.TABLE_CONTACTS,null,null);
                break;



            case R.id.button4:
                if (id.equalsIgnoreCase("")) {
                    textDown.setText("Не указано значение ID");
                    break;
                }

                if (!name.equalsIgnoreCase("")) values.put(DBHelper.KEY_NAME,name);
                if (!email.equalsIgnoreCase(""))values.put(DBHelper.KEY_MAIL,email);

                 if (values.size()!=0){
                    int updCount = database.update(DBHelper.TABLE_CONTACTS, values, DBHelper.KEY_ID + "= ?", new String[]{id});
                    textDown.setText( "updated rows count = " + updCount);
                 }
                 else textDown.setText(" Запись не обновлена!!! Все поля пусты!!!");


                editID.getText().clear();
                editName.getText().clear();
                editMail.getText().clear();

                break;




            case R.id.button5:
                if (id.equalsIgnoreCase("")){
                    break;
                }

                int delCount = database.delete(DBHelper.TABLE_CONTACTS, DBHelper.KEY_ID + "="+ id,null);

                textDown.setText("deleted rows count = "+delCount);

                editID.getText().clear();
                break;

            }

        dbHelper.close();

     }
}
